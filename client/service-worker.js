self.addEventListener('push', e => {
    const data = e.data.json()
    self.registration.showNotification(data.title, {
        body: 'Notified by Belle Belle Victor',
        icon: 'https://angular.io/assets/images/logos/angular/logo-nav@2x.png'
    })
})