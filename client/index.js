const PushPublicKey = 'BE_SvPZQGCfS19xQhxm99ndelcFTfHhxofysc14xFtXc49V7QOY7_luBPqtGq2TdIrV4i-Bam5DyXJNM48C0hhU'

if('serviceWorker' in navigator) {
    send().catch(error => console.error(error))
} 

async function send() {
    /** begin registering service worker */
    const register = await navigator.serviceWorker.register('/service-worker.js', {
        scope: '/'
    })
    /** end registering service worker */

     /** Register push */

     const convertedVapidKey  = urlBase64ToUint8Array(PushPublicKey)
     const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: convertedVapidKey
    });

    /** end register push notification */

    /** Send push notification */

    await fetch('/subscribe', {
        method: 'POST',
        body: JSON.stringify(subscription),
        headers: {
            'content-type': 'application/json'
        }
    })

}

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}